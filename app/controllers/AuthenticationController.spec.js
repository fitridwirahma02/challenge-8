const { Role, User } = require('../../app/models')
const { InsufficientAccessError } = require('../../app/errors');
const AuthenticationController = require('../../app/controllers/AuthenticationController')
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { RecordNotFoundError } = require('../../app/errors');
const { EmailNotRegisteredError, WrongPasswordError } = require('../../app/errors');
const { EmailAlreadyTakenError } = require('../../app/errors');
const { JWT_SIGNATURE_KEY } = require('../../config/application');

describe('authorize function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })

    describe('Successful Operation', () => {
        let token

        beforeAll(() => {
            token = jwt.sign({
                id: 3,
                name: "brian",
                email: "brian@binar.co.id",
                image: null,
                role: {
                    id: 1,
                    name: "CUSTOMER"
                }
            }, JWT_SIGNATURE_KEY)
        })

        afterAll(() => {

        })

        it('(valid jwt token) should hit next endpoint', () => {
            const mReq = { headers: { authorization: `Bearer ${token}` } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            authenticationController.authorize('CUSTOMER')(mReq, mRes, mNext)
            
            expect(mNext).toHaveBeenCalled()
        })
    })

    describe('Error Operation', () => {
        let token

        beforeAll(() => {
            token = jwt.sign({
                id: 3,
                name: "brian",
                email: "brian@binar.co.id",
                image: null,
                role: {
                    id: 1,
                    name: "CUSTOMER"
                }
            }, JWT_SIGNATURE_KEY)
        })

        afterAll(() => {

        })

        it('(empty token) should return status code 401 and return error InsufficientAccessError', () => {
            const mReq = { headers: { authorization: 'Bearer ' } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            authenticationController.authorize('ADMIN')(mReq, mRes, mNext)

            const expectedError = new jwt.JsonWebTokenError("jwt must be provided")
            const expecteResponse = {
                error: {
                    name: expectedError.name,
                    message: expectedError.message,
                    details: null
                }
            }
            expect(mRes.status).toBeCalledWith(401)
            expect(mRes.json).toBeCalledWith(expecteResponse)
        })

        it('(invalid token token) should return status code 401 and return error JsonWebToken', () => {
            const mReq = { headers: { authorization: 'Bearer 123456' } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            authenticationController.authorize('ADMIN')(mReq, mRes, mNext)

            const expectedError = new jwt.JsonWebTokenError("jwt malformed")
            const expecteResponse = {
                error: {
                    name: expectedError.name,
                    message: expectedError.message,
                    details: null
                }
            }
            expect(mRes.status).toBeCalledWith(401)
            expect(mRes.json).toBeCalledWith(expecteResponse)
        })


        it('(invalid rolename) should return status code 401 and return error InsufficientAccessError', () => {
            const mReq = { headers: { authorization: `Bearer ${token}` } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            authenticationController.authorize('ADMIN')(mReq, mRes, mNext)

            const expectedError = new InsufficientAccessError("CUSTOMER")
            const expectedResponse = {
                error: {
                    name: expectedError.name,
                    message: expectedError.message,
                    details: expectedError.details
                }
            }
            expect(mRes.status).toBeCalledWith(401)
            expect(mRes.json).toBeCalledWith(expectedResponse)
        })

        it('(empty parameter) should return status code 401 and return error InsufficientAccessError', () => {
            const mReq = { headers: { authorization: `Bearer ${token}` } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            authenticationController.authorize()(mReq, mRes, mNext)

            const expectedError = new Error("invalid parameter")
            const expectedResponse = {
                error: {
                    name: expectedError.name,
                    message: expectedError.message,
                    details: null
                }
            }

            expect(mRes.status).toBeCalledWith(401)
            expect(mRes.json).toBeCalledWith(expectedResponse)
        })
    })
})

describe('createTokenFromUser function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    describe('Successful Operation', () => {
        let payloadToken
        let user
        let userRole
        beforeAll(async () => {
            user = await userModel.findByPk(1)
            userRole = await roleModel.findByPk(user.roleId)
            payloadToken = {
                id: user.id,
                name: user.name,
                email: user.email,
                image: user.image, 
                role: {
                    id: userRole.id,
                    name: userRole.name,
                }
            }
        })
        
        afterAll(() => {
            
        })
        
        it('(valid user and role) should return jwt token', async () => {
            const expectedToken = jwt.sign(payloadToken, JWT_SIGNATURE_KEY)

            const resultToken = authenticationController.createTokenFromUser(user, userRole)
            expect(resultToken).toEqual(expectedToken)
        })
    })

    describe('Error Operation', () => {
        let payloadToken
        let user
        let userRole


        let wrongUser
        let wrongUserRole
        beforeAll(async () => {
            user = await userModel.findByPk(1)
            userRole = await roleModel.findByPk(user.roleId)
            payloadToken = {
                id: user.id,
                name: user.name,
                email: user.email,
                image: user.image, 
                role: {
                    id: userRole.id,
                    name: userRole.name,
                }
            }

            wrongUser = await userModel.findByPk(5)
            wrongUserRole = await roleModel.findByPk(wrongUser.roleId)
            
        })
        
        afterAll(() => {
            
        })

        it('(invalid user and role) should return invalid jwt token', async () => {
            const expectedToken = jwt.sign(payloadToken, JWT_SIGNATURE_KEY)

            const resultToken = authenticationController.createTokenFromUser(wrongUser, wrongUserRole)
            expect(resultToken).not.toEqual(expectedToken)
        })

        it('(no user and no role) should return error of specific type', async () => {
            const expectedError = new TypeError("Cannot read properties of null (reading 'id')")
            expect(() => authenticationController.createTokenFromUser(null, null)).toThrow(expectedError)
        })
    })
})

describe('decodeToken function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    describe('Successful Operation', () => {
        let user
        let userRole
        let payload
        let token
        beforeAll(async () => {
            user = await userModel.findByPk(1)
            userRole = await roleModel.findByPk(user.roleId)
            payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                image: user.image,
                role: {
                  id: userRole.id,
                  name: userRole.name,
                }
            }

            token = authenticationController.createTokenFromUser(user, userRole)
        })

        afterAll(() => {

        })
        it('(valid token) should return the user object from corresponding token', async () => {
            const resultUser = authenticationController.decodeToken(token)
            expect(resultUser.id).toEqual(payload.id)
            expect(resultUser.name).toEqual(payload.name)
            expect(resultUser.email).toEqual(payload.email)
            expect(resultUser.image).toEqual(payload.image)
            expect(resultUser.role.id).toEqual(payload.role.id)
            expect(resultUser.role.name).toEqual(payload.role.name)
        }) 
    })

    describe('Error Operation', () => {
        let user
        let userRole
        let payload
        let token
        beforeAll(async () => {
            user = await userModel.findByPk(1)
            userRole = await roleModel.findByPk(user.roleId)
            payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                image: user.image,
                role: {
                  id: userRole.id,
                  name: userRole.name,
                }
            }

            token = authenticationController.createTokenFromUser(user, userRole)
        })

        afterAll(() => {

        })

        it('(invalid token) should return error with message JsonWebTokenError', async () => {
            const expectedError = new jwt.JsonWebTokenError("invalid signature")
            const func = () => {
                try{
                    authenticationController.decodeToken(token.slice(0, -1))
                } catch(err){
                    throw err
                }
            }
            expect(func).toThrow(expectedError)
        })

        it('(no token) should return error with message JsonWebTokenError', async () => {
            const expectedError = new jwt.JsonWebTokenError("jwt must be provided")
            const func = () => {
                try{
                    authenticationController.decodeToken()
                } catch(err){
                    throw err
                }
            }
            expect(func).toThrow(expectedError)
        })
    })
})

describe('encryptPassword function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })
        it('(valid password) should return the hashed password', async () => {
            const resultHashedPassword = authenticationController.encryptPassword("123456")
            expect(resultHashedPassword).toBeDefined()
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })
        it('(blank password) should return the hashed password', async () => {
            const expectedError = new Error("Illegal arguments: undefined, string")
            const func = () => {
                try{
                    authenticationController.encryptPassword()
                } catch(err){
                    throw err
                }
            }
            expect(func).toThrow(expectedError)
        })
    })
})

describe('handleGetUser function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    
    describe('Successful Operation', () => {
        let user
        let userRole
        beforeAll(async () => {
            user = await userModel.findByPk(1)
            userRole = await roleModel.findByPk(user.roleId)
        })
        
        afterAll(() => {
            
        })

        it('(valid jwt token) should return status code 200 and return the user object of corresponding jwt token payload', async () => {
            const mReq = { user: { id: user.id } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleGetUser(mReq, mRes, mNext)
            
            const expectedResponse = {
                id: user.id,
                name: user.name,
                email: user.email,
                image: user.image,
                role: {
                    id: userRole.id,
                    name: userRole.name
                }
            }
            expect(mRes.status).toBeCalledWith(200)
            expect(mRes.json).toBeCalledWith(expectedResponse)  
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {
            
        })
        
        afterAll(() => {
            
        })

        it('(no user found) should return status code 404 and return error with message RecordNotFoundError', async () => {
            const mReq = { user: { id: 0 } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleGetUser(mReq, mRes, mNext)
            
            const expectedResponse = new RecordNotFoundError()
            expect(mRes.status).toBeCalledWith(404)
            expect(mRes.json).toBeCalledWith(expectedResponse)
        })
    })
})

describe('handleLogin function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    
    describe('Successful Operation', () => {
        beforeAll(() => {
            
        })
        
        afterAll(() => {
            
        })
        
        it('(valid email and password) should return status code 201 and return accessToken of the corresponding logged in user', async () => {
            const userCredential = {
                email: "brian@binar.co.id",
                password: "123456"
            }
            const mReq = { body: userCredential }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleLogin(mReq, mRes, mNext)
            
            expect(mRes.status).toBeCalledWith(201)
            expect(mRes.json).toBeCalledWith(
                expect.objectContaining({
                    accessToken: expect.any(String),
                })
            )
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {
            
        })
        
        afterAll(() => {
            
        })
        
        it('(not registered email) should return status 404 and return error EmailNotRegisteredError', async () => {
            const userCredential = {
                email: "testing@binar.com",
                password: "123456"
            }
            const mReq = { body: userCredential }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleLogin(mReq, mRes, mNext)
            
            const expectedError = new EmailNotRegisteredError(mReq.body.email)
            expect(mRes.status).toBeCalledWith(404)
            expect(mRes.json).toBeCalledWith(expectedError)
        })

        it('(wrong password) should return status 401 and return error WrongPasswordError', async () => {
            const userCredential = {
                email: "brian@binar.co.id",
                password: "brian"
            }
            const mReq = { body: userCredential }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleLogin(mReq, mRes, mNext)
            
            const expectedError = new WrongPasswordError()
            expect(mRes.status).toBeCalledWith(401)
            expect(mRes.json).toBeCalledWith(expectedError)
        })

        it('(empty body) should hit handleError function', async () => {
            const mReq = { body: {  } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()

            await authenticationController.handleLogin(mReq, mRes, mNext)
            
            expect(mNext).toHaveBeenCalled()
        })
    })
})

describe('handleRegister function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(async () => {
            const users = await userModel.findAll({
                limit: 1,
                order: [ ['createdAt', 'DESC' ] ]
            })
            
            const userId = users[0].id
            await userModel.destroy({ where: { id: userId } })
        })

        it('(valid new email) should return status code 201 and return accessToken of the new registered user', async () => {
            const userCredential = {
                name: "Testing One",
                email: 'testing123@binar.co.id',
                password: "123456"
            }
            
            const mReq = { body: userCredential }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleRegister(mReq, mRes, mNext)
            
            expect(mRes.status).toBeCalledWith(201)
            expect(mRes.json).toBeCalledWith(
                expect.objectContaining({
                    accessToken: expect.any(String),
                })
            )
        })
    })

    describe('Error Operation', () => {
        it('(existed email) should return status code 422 and return error EmailAlreadyTakenError', async () => {
            const userCredential = {
                name: 'Brian',
                email: 'brian@binar.co.id',
                password: '123456'
            }
            
            const mReq = { body: userCredential }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            await authenticationController.handleRegister(mReq, mRes)

            const expectedError = new EmailAlreadyTakenError(mReq.body.email)
            expect(mRes.status).toBeCalledWith(422)
            expect(mRes.json).toBeCalledWith(expectedError)
        })

        it('(empty body) should hit handleError function', async () => {
            const mReq = { body: {  } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()

            await authenticationController.handleRegister(mReq, mRes, mNext)
            
            expect(mNext).toHaveBeenCalled()
        })
    })
})

describe('verifyPassword function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    const expectedPassword = "123456"
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })
        it('(valid password) should return true', async () => {
            const password = "123456"
            const hashedPassword = bcrypt.hashSync(password, 10)
            const result = authenticationController.verifyPassword(expectedPassword, hashedPassword)
            expect(result).toBeTruthy()
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })
        it('(blank password) should return error of specific type', async () => {
            const password = null
            const expectedError = new Error(`Illegal arguments: ${typeof password}, string`)

            const func = () => {
                try{
                    const hashedPassword = bcrypt.hashSync(password, 10)
                    authenticationController.verifyPassword(expectedPassword, hashedPassword)
                } catch(err){
                    throw err
                }
            }
            expect(func).toThrow(expectedError)
        })

        it('(invalid password) should return false', async () => {
            const password = ""
            const hashedPassword = bcrypt.hashSync(password, 10)
            const result = authenticationController.verifyPassword(expectedPassword, hashedPassword)
            expect(result).not.toBeTruthy()
        })
    })
})
