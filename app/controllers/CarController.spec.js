const { CarController } = require('../../app/controllers')
const { Car, UserCar } = require('../../app/models')
const dayjs = require('dayjs')
const { CarAlreadyRentedError } = require('../../app/errors')

describe('getCarFromRequest function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })

        it('(valid request params) should return one car object', async () => {
            const resultResponse = await carController.getCarFromRequest({
                params: {
                    id: 1
                }
            })

            const expectedResponse = await carModel.findByPk(1)
            expect(resultResponse).toEqual(expectedResponse)
        })
    })

    describe('Error Operation', () => {
        
    })
})

describe('getCarFromRequest function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })

        it('(valid params query with page, pageSize, size and availableAt) should return list query object', async () => {
            const query = {
                size: "SMALL",
                availableAt: new Date(),
                page: 2, 
                pageSize: 15,

            }
            const resultQuery = carController.getListQueryFromRequest({ query })

            expect(resultQuery.include.where).toBeDefined()
            expect(resultQuery.where).toEqual({ size: query.size })
            expect(resultQuery.limit).toEqual((query.page - 1) * query.pageSize)
            expect(resultQuery.offset).toEqual(query.pageSize)
        })
        
    })

    describe('Error Operation', () => {

    })
})

describe('handleGetCar function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(async () => {
            const cars = await carModel.findAll({
                limit: 1,
                order: [ ['createdAt', 'DESC' ] ]
            })
            
            const carId = cars[0].id
            await carModel.destroy({ where: { id: carId } })
        })

        it('(valid request body) should return status code 201 and return a new created car', async () => {
            const newCar = {
                name: "Testing 1",
                price: 1250000,
                size: "SMALL",
                image: "https://source.unsplash.com/531x531"
            }
            const mReq = { body: newCar }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleCreateCar(mReq, mRes, mNext)


            expect(mRes.status).toBeCalledWith(201)
            expect(mRes.json).toBeDefined()
        })
    })

    describe('Error Operation', () => {

    })
})

describe('handleDeleteCar function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        let car
        beforeAll(async () => {
            car = await carModel.create({
                name: "Testing 1",
                price: 1250000,
                size: "SMALL",
                image: "https://source.unsplash.com/531x531"
            })            
        })

        afterAll(() => {
            
        })

        it('(valid request body and params) should return status code 204 and return the deleted car', async () => {
            const mReq = { params: { id: car.id } }
            const mRes = { status: jest.fn().mockReturnThis(), end: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleDeleteCar(mReq, mRes, mNext)
            expect(mRes.status).toBeCalledWith(204)
        })
    })

    describe('Error Operation', () => {
       
    })
})

describe('handleGetCar function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })
        it('(valid request params) should return status code 200 and return a car object', async () => {
            const mReq = { params: { id: 1 } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleGetCar(mReq, mRes, mNext)


            const expectedResponse = await carModel.findOne({ where: mReq.params })
            expect(mRes.status).toBeCalledWith(200)
            expect(mRes.json).toBeCalledWith(expectedResponse)
        })
    })

    describe('Error Operation', () => {

    })
})

describe('handleListCars function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })
        it('(valid request with offset, pageSize, size and availableAt) should return status code 200 and return lists of cars', async () => {
            const mReq = { query: { page: 2, pageSize: 5, size: "SMALL", availableAt: new Date(new Date().setDate(new Date(). getDate() - 100)) } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleListCars(mReq, mRes, mNext)

            
            expect(mRes.status).toBeCalledWith(200)
            expect(mRes.json).toBeDefined()
        })
    })

    describe('Error Operation', () => {

    })
})

describe('handleRentCar function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(async () => {
            const userCars = await userCarModel.findAll({
                limit: 1,
                order: [ ['createdAt', 'DESC' ] ]
            })
            
            const userCarId = userCars[0].id
            await userCarModel.destroy({ where: { id: userCarId } })
        })

        it('(valid request params and body) should return status code 201 and return a new rented car details', async () => {
            const newRentDetail = {
                rentStartedAt: new Date()
            }
            const mReq = { body: newRentDetail, params: { id: 1 }, user: { id: 1 } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleRentCar(mReq, mRes, mNext)

            expect(mRes.status).toBeCalledWith(201)
            expect(mRes.json).toBeDefined()
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {

        })

        afterAll(async () => {
            const userCars = await userCarModel.findAll({
                limit: 1,
                order: [ ['createdAt', 'DESC' ] ]
            })
            
            const userCarId = userCars[0].id
            await userCarModel.destroy({ where: { id: userCarId } })
        })

        it('(car already rented) should return 422 and return error CarAlreadyRented', async () => {
            const mReq1 = { body: { rentStartedAt: new Date(new Date().setDate(new Date(). getDate())), rentEndedAt: new Date(new Date().setDate(new Date(). getDate() + 1)) }, params: { id: 1 }, user: { id: 1 } }
            const mRes1 = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext1 = jest.fn()
            await carController.handleRentCar(mReq1, mRes1, mNext1)
            
            
            const mReq2 = mReq1
            const mRes2 = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext2 = jest.fn()
            await carController.handleRentCar(mReq2, mRes2, mNext2)

            const car = await carModel.findByPk(1)
            const expectedError = new CarAlreadyRentedError(car)
            expect(mRes2.status).toBeCalledWith(422)
            expect(mRes2.json).toBeCalledWith(expectedError)
        })

        it('(empty request params) should hit handleError function', async () => {
            const mReq = { body: { rentStartedAt: new Date(new Date().setDate(new Date(). getDate())) }, params: {  }, user: { id: 1 } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleRentCar(mReq, mRes, mNext)

            expect(mNext).toHaveBeenCalled()
        })
        it('(empty request user) should hit handleError function', async () => {
            const mReq = { body: { rentStartedAt: new Date(new Date().setDate(new Date(). getDate())) }, params: { id: 1 }}
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleRentCar(mReq, mRes, mNext)

            expect(mNext).toHaveBeenCalled()
        })
    })
})

describe('handleUpdateCar function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        let car
        beforeAll(async () => {
            car = await carModel.create({
                name: "Testing 1",
                price: 1250000,
                size: "SMALL",
                image: "https://source.unsplash.com/531x531"
            })            
        })

        afterAll(async () => {
            await carModel.destroy({ where: { id: car.id } })
        })

        it('(valid request body and params) should return status code 200 and return the updated car', async () => {
            const updatedCar = {
                name: "Testing 2",
                price: 150000000,
                size: "LARGE",
                image: "https://source.unsplash.com/500x500"
            }
            const mReq = { body: updatedCar, params: { id: car.id } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleUpdateCar(mReq, mRes, mNext)
            expect(mRes.status).toBeCalledWith(200)
            expect(mRes.json).toBeDefined()
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })
        it('(invalid params id) should return 422 and return error of specific type', async () => {
            const updatedCar = {
                name: "Testing 2",
                price: 150000000,
                size: "LARGE",
                image: "https://source.unsplash.com/500x500"
            }
            const mReq = { body: updatedCar, params: {  } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleUpdateCar(mReq, mRes, mNext)
            
            const error = new TypeError("Cannot read properties of null (reading 'update')")
            const expectedError = {
                error: {
                    message: error.message,
                    name: error.name
                }
            }
            expect(mRes.status).toBeCalledWith(422)
            expect(mRes.json).toBeCalledWith(expectedError)
        })
    })
})
