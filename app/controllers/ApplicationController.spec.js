const ApplicationController = require('../../app/controllers/ApplicationController')
const { EmailAlreadyTakenError } = require('../../app/errors')
const { NotFoundError } = require('../../app/errors')

describe('buildPaginationObject function', () => {
    const applicationController = new ApplicationController()
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('(empty request query) should return an pagination object', async () => {
            const query = { }
            const expectedPagination = {
                page: 1,
                pageCount: Math.ceil(50 / 10),
                pageSize: 10,
                count: 50
            }
            const resultPagination = applicationController.buildPaginationObject({ query }, expectedPagination.count)

            expect(expectedPagination).toEqual(resultPagination)
        })
    })

    describe('Error Operation', () => {
        
    })
})

describe('getOffsetFromRequest function', () => {
    const applicationController = new ApplicationController()
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('(valid request query) should return an offset', async () => {
            const query = { page: 2, pageSize: 5 }
            const expectedOffset = (query.page - 1) * query.pageSize
            const resultOffset = applicationController.getOffsetFromRequest({ query })

            expect(expectedOffset).toEqual(resultOffset)
        })
    })

    describe('Error Operation', () => {
        
    })
})

describe('handleNotFound function', () => {
    const applicationController = new ApplicationController()
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('(valid request) should return status code 500 and return error with message Internal Server Error', async () => {
            const mReq = { }
            const err = new EmailAlreadyTakenError("fikri@binar.co.id")
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            applicationController.handleError(err, mReq, mRes, mNext)

            const expectedResult = {
                error: {
                    name: err.name,
                    message: err.message,
                    details: err.details
                }
            }
            expect(mRes.status).toBeCalledWith(500)
            expect(mRes.json).toBeCalledWith(expectedResult)
        })
    })

    describe('Error Operation', () => {
        
    })
})

describe('handleGetRoot function', () => {
    const applicationController = new ApplicationController()
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('(valid request) should return status code 200 and return status OK', async () => {
            const mReq = {}
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            applicationController.handleGetRoot(mReq, mRes, mNext)

            const expectedResult = {
                status: "OK",
                message: "BCR API is up and running!",
            }
            expect(mRes.status).toBeCalledWith(200)
            expect(mRes.json).toBeCalledWith(expectedResult)
        })
    })

    describe('Error Operation', () => {

    })
})

describe('handleNotFound function', () => {
    const applicationController = new ApplicationController()
    
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('(valid request) should return status code 404 and return error NotFoundError', async () => {
            const mReq = { method: "GET", url: "/hello" }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            applicationController.handleNotFound(mReq, mRes, mNext)

            const err = new NotFoundError(mReq.method, mReq.url)
            const expectedResult = {
                error: {
                    name: err.name,
                    message: err.message,
                    details: err.details
                }
            }
            expect(mRes.status).toBeCalledWith(404)
            expect(mRes.json).toBeCalledWith(expectedResult)
        })
    })

    describe('Error Operation', () => {
        
    })
})

